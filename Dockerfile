FROM rpicluster/armtomcat 

ENV CATALINA_HOME /usr/share/tomcat8/
ENV CATALINA_BASE /var/lib/tomcat8/

EXPOSE 8080
EXPOSE 8005

ENTRYPOINT ["/usr/share/tomcat8/bin/catalina.sh","run"]
